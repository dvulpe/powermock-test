package com.acme.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(MyServiceLocator.class)
public class MyServiceLocatorTest {

    @Mock
    private ServiceLocator serviceLocatorMock;
    @Mock
    private ResourceService resourceServiceMock;

    @Before
    public void setUp() throws Exception {
        mockStatic(MyServiceLocator.class);
        when(MyServiceLocator.getLocator()).thenReturn(serviceLocatorMock);

        when(serviceLocatorMock.getResourceService()).thenReturn(resourceServiceMock);
        when(resourceServiceMock.getDefaultResource()).thenReturn("DEFAULT");
    }

    @Test
    public void testPowerMockShouldBeUsed() throws Exception {
        Assert.assertEquals("DEFAULT", MyServiceLocator.getLocator().getResourceService().getDefaultResource());
    }
}
