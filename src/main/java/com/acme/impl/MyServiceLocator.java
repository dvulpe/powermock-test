package com.acme.impl;

public class MyServiceLocator implements ServiceLocator {

    public static ServiceLocator getLocator() {
        return new MyServiceLocator();
    }

    @Override
    public ResourceService getResourceService() {
        return new ResourceService() {
            @Override
            public String getDefaultResource() {
                return "N/A";
            }
        };
    }
}
