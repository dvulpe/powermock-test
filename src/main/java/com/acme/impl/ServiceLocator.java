package com.acme.impl;

public interface ServiceLocator {
    ResourceService getResourceService();
}
